# Installation
ToDo: Put some instructions on how to install the git repository.
## Requirements
1. [Google App Engine SDK](https://cloud.google.com/appengine/downloads)
2. Python 2.7 (pre-installed in Google App Engine)
3. PIP (pre-installed on Google App Engine)
4. Have a mySQL server (version 5.7) running on the machine

## Instructions
1. Install Google App Engine SDK on development machine
2. Clone Git Repository
3. Add as project from pre-existing files on PyCharm or similar
4. Install requirements through PIP
    1. On Windows:
      ```python -m pip install -r requirements.txt -t lib/```
        1. If Python < v2.7.9 first install pip by running ```python get-pip.py/```
    2. Else:
        ```pip install -r requirements.txt -t lib/```
5. Create a mysql database instance

## Running the Development Environment
1. Clone a copy of `db_example.py` and name it `db_local.py`
1. Ensure that the local mysql database is running
1. Change the ports / database name / username / password and any other parameters needed to run on local machine in the new `db_local.py` file
1. Run the `manage.py` script to populate the database. Make sure to set the `SERVER_SOFTWARE` environment variable to be `Development` before running the scripts.

    `export SERVER_SOFTWARE='Development'`

    `python manage.py db upgrade`
1. Run the Google App Engine developer from either your IDE, or through the command<br/>
 `dev_appserver.py --port=8080 main.py`


# File Structure
- `.gitignore`: Files ignored by git
- `app.yaml`: Specifies the application's runtime configuration on Google App Engine
- `appengine_config.py`: GAE configuration
- `main.py`: Runs the application (I guess)
- `manage.py`: A script to run database migrations and such things
- `readme.md`: Well, this file :)
- `requirements.txt`: A list of all third-party dependencies for this project.
- `configs`: Configuration files
    - `.gitignore`: Files ignored by git
    - `__init__.py`:  *empty, can be deleted?*
    - `db.py`: Specifies from which file in `configs` the database flask configs should be loaded, depending on the environment.
    - `db_example.py`: A template which is cloned and named to `db_local.py` and modified to reflect the ports / database name / username / password and any other parameters needed to run on the local machine (see Running the Development Environment section above).
    - `db_local.py`: Specifies the flask config to load the database in the local environment.
    - `db_prod.py`: Specifies the flask config to load the database in the production environment.
    - `flask.py`: [Flask](http://flask.pocoo.org/docs/0.12/) configuration.
- `lib`: Library files
- `migrations`: Change structure and/or content of the database:
    - `db_data`: csv files which contain the data to upgrade/downgrade the database content with.
    - `versions`: migration files which specify how to upgrade/downgrade the database for the latest updates.
    - `__init__.py`: *empty, can be deleted?*
    - `alembic.ini`: [Alembic](http://alembic.zzzcomputing.com/en/latest/) configuration.
    - `env.py`: A python script that is run whenever the alembic migration tool is invoked. [See more](http://alembic.zzzcomputing.com/en/latest/tutorial.html#the-migration-environment)
    - `README`: Info on how to create a migration file, csv files, and how to run upgrades/downgrades to the database.
    - `script.py.mako`: A [Mako](http://www.makotemplates.org/) template file which is used to generate new migration scripts.  Whatever is here is used to generate new files within `versions/`. This is scriptable so that the structure of each migration file can be controlled, including standard imports to be within each, as well as changes to the structure of the `upgrade()` and `downgrade()` functions.
- `sherpa`: Everything Sherpa-specific
    - `__init__.py`: Just defines the `__all__` variable which tells python what to import when it's told to import * from `sherpa`
    - `api`: API-related stuff
        - `docs`: API documentation using [Flasgger](https://github.com/rochacbruno/flasgger), an easy [Swagger](https://swagger.io/) UI for your [Flask](http://flask.pocoo.org/docs/0.12/) API.
            - `static`: Contains the yml documentation files in folders depending on which version of the API the route appears in.
                - `missing_swag.yml`: Documentation to display for when the documentation file corresponding to a particular HTML function is missing.
            - `__init__.py`: Sets up Flasgger and its settings for API documentation
        - `validation`: Serialisation, validation, and much more based on [marshmallow](https://marshmallow.readthedocs.io/en/latest/)
            - `__init__.py`: Some custom extensions of Marshmallow's Schema, and Fields.
            - rest of the files: schemas to serialise several objects in the Sherpa model from the JSON request.
        - `__init__.py`: sets up the [Flask](http://flask.pocoo.org/docs/0.12/) API
        - `api.py`: The collection of API resources defined.
        - `auth.py`: API authorisation
        - `resources.py`: Extensions and convolutions of Flask's Resource
        - `response.py`: Defines a Response class which handles how the API returns the result of the HTML functions.
    - `errors`: Error related stuff
        - `__init__.py`: Defines the Errors class, which is our error-handling mechanism, and the SherpaException, which extends Exception and whose instances will be caught and returned through the API.
        - rest of the files: Definitions of classes extending SherpaExceptions.
    - `model`: The Sherpa model
        - `__init__.py`: Just defines the `__all__` variable which tells python what to import when it's told to import * from `model`.
        - `risk_protection_score.py`: Responsible for
        - `db`: The Database model
            - `__init__.py`: Sets up Caching, and defines the `AlchemyEncoder` class, which extends `JSONEncoder` and provides further specifications on how to encode certain objects to json.
            - rest of the files: Definitions of the database model, where each class represents a table in the database, with methods specifying how to return data from the table.
        - `events`:
            - `__init__.py`: Defines the generic Event classes
            - rest of the files: Define the specific Event classes
        - `household`:
            - `__init__.py`: Defines the Household object
            - `insurable.py`: Defines an Insurable object
            - `activities`:
                - `__init__.py`: Defines activities objects that the household might partake in.
            - `financials`:
                - `__init__.py`: Defines the Financials object, storing all household financial info
                - `assets`:
                    - `__init__.py`: Defines an abstract Asset class, and an Assets object which stores all the household's assets
                    - `assets.py`: Defines the different kind of assets and related classes.
                - `persons`:
                    - `__init__.py`: Defines an abstract Person class, and a Persons object which stores all the household's persons
                    - `persons.py`: Defines the different kind of persons and related classes.
                - `pets`:
                    - `__init__.py`: Defines the Pet clsas
                - `policies`:
                    - `__init__.py`: Defines abstract policy classes, and a Policies object which stores all the household's policies.
                    - `flow_payment.py`: Defines all policy classes that are of flow payment type.
                    - `lump_sum.py`: Defines all policy classes that are of lump sum payment type.
        - `modules`: Code related to calculating the values for the four modules: FI, FR, IC, and P
            - `__init__.py`: Defines generic module classes that contain logic common to the calculation of all four modules.
            - rest of the files: Define module classes particular to specific events that contain logic common to the calculation of all four modules.
            - rest of the folders: Code related to calculating the values for the individual modules
                - `__init__.py`: Defines generic module-specific classes that contain logic common to the calculation of the specific module for all or several classes.
                - rest of the files: Define the classes responsible for calculating the value of the specific module for each event.
    - `utils`: Various utilities.
        - `__init__.py`: Just defines the `__all__` and `unset` variables.
        - rest of the files: Define several other utility methods and classes.
    - `templates`: Pages to show in case something breaks.
    - `tests`: Legacy testing scripts in javascript

# API Documentation
## How to call the methods
## Methods Response
The API will return back a JSON object with the following fields.

| Name | Type | Description |
| --- | --- | --- |
| code | integer | A code representing the output of the call. The full list of these can be found by calling [get_errors](#get_errors) method.
| error | array | An array containing any errors that might have occurred with the call. This field will not be returned if there's no problems. |
| response | object | The output of the call. The value of this is defined in each of the respective method documentation.
| query | object | A copy of the input parameters passed to the query, post validation and cleanup. |


## Methods
### Get Defaults
Returns the default values generated by the system for a given household input data set
#### URL
[/api/v1.0/defaults/](/api/v1.0/defaults/)
#### Variables
##### Household
Required
### Get Events
Retrieves all the events and child events, in a hierarchical mode
#### URL
[/api/v1.0/events/](/api/v1.0/events/)
#### Parameters
| Name | Type | Default | Description | 
| --- | --- | --- | --------- |
| id | integer | 0 | The ID of the event to retrieve the event and child events list for.<br/>To return all events, this value should not be passed or set as 0 |
| limit | integer | 0 | The limit of depth in the tree to retrieve the list for.<br/>To return all items in the tree, this value should not be passed or set as 0.<br/>To return a single event, the limit should be 1.<br/>dTo return the single event and the direct children of the event, limit should be set to 2, etc.

#### Returns
Returns a json object with an array of events, with each of the array items containing the below.

| Name | Type | Description |
| --- | --- | --- |
| id | integer | ID of the event |
| parent | integer | ID of the parent of the event |
| name | string | Name of the event |

### Risk Protection Score
Calculates the RPS of an event for a household
#### URL
[/api/v1.0/rps/](/api/v1.0/rps/)
#### Input Format

For a description of what goes into each `#vars#`, see the Variables section below.

    {
      #vars#,
      "household":{
        #vars#,
        "persons":{
          "adults":[{#vars#}(,{#vars#},...)],
          "children":[({#vars#},{#vars#},...)]
        },
        "pets":[({#vars#},{#vars#},...)]
        "financials":{
          #vars#,
          "stage_1_tiers": [{#vars#},{#vars#},...],
          "stage_1_limits": {#vars#},
          "assets": {
            "vehicles": [({#vars#},{#vars#},...)],
            "residences": [{#vars#}, ({#vars#}, ... )],
            "emergency_cash_sources": [({#vars#},{#vars},...)]
        }
        "policies":{
          #policy_code#:[{
            "insured": [{#vars}(,{#vars},...)}],
            #vars#
            },
            {...},
            ...],
          ...
        }
      }
    }

#### Variables
##### RPS

| Name | Type | Required | Description |
| --- | --- | --- | --- |
| household | object | yes | See Household section below.|

##### Household
Required

| Name | Type | Required | Description |
| --- | --- | --- | --- |
| persons | object | yes | See Persons section below.|
| pets | list[object] | no | See Pet section below.
| financials | object | no | See Financials section below. |

##### Persons

| Name | Type | Required | Description |
| --- | --- | --- | --- |
| adults | list[object] | yes | See Adults section below.|
| children | list[object] | yes | See Children section below.|

#### Adults
At least one adult is required

| Name | Type | Required | Description |
| --- | --- | --- | --- |
| id | string | yes | Unique identifier |
| dob | yyyy/mm/dd (ISO 8601, but dd/mm/yyyy also seems to work) | Yes | Date of Birth|
| income | integer >= 0| yes | Net monthly income |
| potential_income| integer >= 0| no | Potential net monthly income if necessary |
| gender | "male"/"female"/"undefined" | no | Gender |
| travel | object | no | See Travel below. |
| occupation_industry | integer | no | ID of occupation industry |
| smoker | boolean | no | Smoker or not |

##### Children
Only required if there are children.

| Name | Type | Required | Description |
| --- | --- | --- | --- |
| id | string | yes | Unique identifier |
| dob | yyyy/mm/dd (ISO 8601, but dd/mm/yyyy also seems to work) | Yes | Date of Birth|
| gender | "male"/"female"/"undefined" | no | Gender |
| upbringing_costs | float >= 0 | no | Total costs for bringing up the child (excluding childcare, private education, and university) |
| paid_childcare| boolean | no | pay for childcare? |
| childcare_cost | float >= 0 | no | Total cost for childcare |
| private_education | boolean | no | pay for private education? |
| private_education_cost | float >= 0 | no | Total cost for private education. |
| paid_university | boolean | no | pay for university? |
| university_cost | float >= 0 | no | Total cost for university. |

##### Travel

| Name | Type | Required | Description |
| --- | --- | --- | --- |
| days_abroad | integer >= 0 | yes | How many days expected to spend abroad. |
| travel_region | integer | yes | ID of worldwide region most often travelled to. |
| number_of_trips | integer >= 0 | yes | Expected number of trips per year. |
| luggage_value | float >= 0 | no | Usual value of luggage. |
| winter_sports | boolean | no | Does the adult do winter sports in any trip during the year? |

##### Pets

| Name | Type | Required | Description |
| --- | --- | --- | --- |
| id | string | yes | Unique identifier |
| breed_id | integer | yes | ID of the breed of the pet. |
| age | integer >= 0 | yes | Age of the pet |


##### Financials
Entirely non-required.

| Name | Type | Required | Description |
| --- | --- | --- | --- |
| debt | float >= 0 | no | Current household debt. |
| cash_savings | float >= 0 | no | Bank cash savings. This field is required in order to get a score larger than 0 |
| liquid_assets | float >= 0 | no | Savings in easy to access e.g. ISAs, share schemes, bank accounts overseas, etc |
| stage_1_tiers | list[object] | no | See Stage 1 Tiers section below. |
| stage_1_limit | float between 0 and 100 | no | See Stage 1 Limits section below. |
| assets | list[object] | no | See Assets section below. |
| discretionary_lump | float >= 0 | no | Discretionary lump to leave on death of any (or combination) of adults |
| remaining_mortgage | float >= 0 | no | The cost incurred were the remaining mortgage on all residences to be paid off instantaneously |
| monthly_rent | float >= 0 | no | Current outgoing monthly rent of the household on all residences. |
| percentage_of_remaining_mortgage_cleared_on_death | float between 0 and 100 | The percentage of the remaining mortgage to be cleared off on death. |
| cost_of_funeral | float >= 0 | no | The amount the household would pay for a funeral. |
| cost_of_estate_administration | float >= 0 | no | The amount the household would pay for having their estate administered after a death. |

###### Assets

| Name | Type | Required | Description |
| --- | --- | --- | --- |
| emergency_cash_sources | list[object] | no | See Emergency Cash Source below. |
| vehicles | list[object] | no | See Vehicle below. |
| residences | list[object] | no | See Residence below. |


###### Emergency Cash Source

| Name | Type | Required | Description |
| --- | --- | --- | --- |
| id | string | yes | ID. |
| worth | float >= 0 | yes | Amount of cash. |
| percentage | float between 0 and 100 | yes | Percentage of this cash the user is willing to make use of in case of emergencies. |
| event_ids | list[int] | yes | List of event IDs for which this cash can be used. |

###### Vehicle

| Name | Type | Required | Description |
| --- | --- | --- | --- |
| id | string | yes | ID. |
| worth | float >= 0 | no | Worth of the vehicle |
| percentage | float between 0 and 100 | no | What percentage of the vehicle can be sold off / replaced by a cheaper vehicle in case of an emergency. |
| event_ids | list[int] | no | List of event IDs for which the vehicle can be used as an emergency source of FR in case of need. |
| driven_by | list[object] | yes | List of `[{"id": adult_id, "type": "adult"}]` adults who drive this vehicle. |
| brand_id | integer | no | ID of the brand of the car. |
| model_id | integer | no | ID of the model of the car. |
| age | integer >= 0 | yes | Age of the car in years. |

###### Residence

| Name | Type | Required | Description |
| --- | --- | --- | --- |
| id | string | yes | ID. |
| postcode | String (a valid postcode) | No | The postcode of the residence.|
| housing_situation | String | yes | One of "renting", "rent_free", "mortgage", "outright". |
| contents | object | no | See Contents below. |
| building | object | no | See Building below. |
| floors | integer > 0 | no | Number of floors |
| main | boolean | no | Is this the primary residence of the household? |

###### Contents

| Name | Type | Required | Description |
| --- | --- | --- | --- |
| worth | float | no | Total worth of all the contents |
| immovable_valuables_worth | float >= 0 | no | Total worth of all valuables which are not taken out of the house. |
| portable_valuables_worth | float >= 0 | no | Total worth of all valuables which are regularly taken out of the house. |
| portable_valuables_portability | string | no | One of "every_day" (every day), "other_day" (every other day), "once_week" (once a week), "once_month" (once a month), "less_month" (less than once a month) |
| percentage | float between 0 and 100 | yes | What percentage of the contents can be sold off in case of an emergency. |
| event_ids | list[int] | yes | List of event IDs for which any cash obtained by selling off contents can be used. |


###### Building

| Name | Type | Required | Description |
| --- | --- | --- | --- |
| worth | float >= 0 | no | Total worth of the building |
| percentage | float between 0 and 100 | yes | Can the household sell their building / move to a cheaper one? What percentage of the original worth would the new one cost? |
| event_ids | list[int] | yes | List of event IDs for which any cash obtained by selling / replacing building can be used. |

###### Stage 1 Tiers

| Name | Type | Required | Description |
| --- | --- | --- | --- |
| percentage | float between 0 and 100 | yes | Percentage size of tier. |
| pain_factor | float | yes | Pain factor of tier. |


##### Policies

| Name | Type | Required | Description |
| --- | --- | --- | --- |
| MPI | list[object] | no | See MPI below. |
| TLI | list[object] | no | See TLI below. |
| FIB | list[object] | no | See MPI below. |
| WLI | list[object] | no | See TLI below. |
| DTLI | list[object] | no | See MPI below. |
| IPI | list[object] | no | See TLI below. |
| ASU | list[object] | no | See MPI below. |
| MPPI | list[object] | no | See TLI below. |
| PPI | list[object] | no | See MPI below. |
| ESP | list[object] | no | See TLI below. |
| CI | list[object] | no | See MPI below. |
| HBP | list[object] | no | See HBP below. |
| HCP | list[object] | no | See HCP below. |
| TRP | list[object] | no | See TRP below. |
| PET | list[object] | no | See PET below. |
| CAR | list[object] | no | See CAR below. |


###### Insured
Required for each policy in each policy type below

| Name | Type | Required | Description |
| --- | --- | --- | --- |
| id | string | yes | The ID of the object/person being insured. Has to match the ID of the same object elsewhere in the household. |
| type | string | yes | Type of object/person being insured. |

###### MPI

Entirely non-required

| Name | Type | Required | Description |
| --- | --- | --- | --- |
| id | string | yes | Policy ID |
| amount | float >= 0 | yes | The coverage for the policy |
| excess | float >= 0 | no | Excess. |
| insured | list[object] | yes | List of adults insured (see Insured schema above) |

###### TLI

| Name | Type | Required | Description |
| --- | --- | --- | --- |
| id | string | yes | Policy ID |
| amount | float >= 0 | yes | The coverage for the policy |
| excess | float >= 0 | no | Excess. |
| insured | list[object] | yes | List of adults insured (see Insured schema above) |

###### FIB

| Name | Type | Required | Description |
| --- | --- | --- | --- |
| id | string | yes | Policy ID |
| amount | float >= 0 | yes | The monthly coverage for the policy |
| end_of_term | date in future | yes | The term end date |
| insured | list[object] | yes | List of adults insured (see Insured schema above) |

###### WLI

| Name | Type | Required | Description |
| --- | --- | --- | --- |
| id | string | yes | Policy ID |
| amount | float >= 0 | yes | The coverage for the policy |
| excess | float >= 0 | no | Excess. |
| insured | list[object] | yes | List of adults insured (see Insured schema above) |

###### DTLI

| Name | Type | Required | Description |
| --- | --- | --- | --- |
| id | string | yes | Policy ID |
| amount | float >= 0 | yes | The start of term coverage for the policy |
| start_of_term | date in past | yes | The term start date |
| end_of_term | date in future | yes | The term end date |
| insured | list[object] | yes | List of adults insured (see Insured schema above) |

###### IPI

| Name | Type | Required | Description |
| --- | --- | --- | --- |
| id | string | yes | Policy ID |
| proportion_of_income | float between 0 and 100 | yes | The proportion of the insured person's income they will receive as a monthly coverage |
| excess_duration | integer >= 0 | yes | Number of months after which payments start being received |
| sum_cap | float >= 0 | yes | Maximum possible sum received throughout the whole term |
| months_covered | integer >= 0 | yes | For how many months will payments be received after the excess duration is over |
| insured | list[object] | yes | List of adults insured (see Insured schema above) |

###### ASU

| Name | Type | Required | Description |
| --- | --- | --- | --- |
| id | string | yes | Policy ID |
| proportion_of_income | float between 0 and 100 | yes | The proportion of the insured person's income they will receive as a monthly coverage |
| excess_duration | integer >= 0 | yes | Number of months after which payments start being received |
| sum_cap | float >= 0 | yes | Maximum possible sum received throughout the whole term |
| months_covered | integer >= 0 | yes | For how many months will payments be received after the excess duration is over |
| insured | list[object] | yes | List of adults insured (see Insured schema above) |

###### MPPI

| Name | Type | Required | Description |
| --- | --- | --- | --- |
| id | string | yes | Policy ID |
| amount | float >= 0 | yes | The monthly coverage |
| excess_duration | integer >= 0 | yes | Number of months after which payments start being received |
| sum_cap | float >= 0 | yes | Maximum possible sum received throughout the whole term |
| months_covered | integer >= 0 | yes | For how many months will payments be received after the excess duration is over |
| insured | list[object] | yes | List of adults insured (see Insured schema above) |

###### PPI

| Name | Type | Required | Description |
| --- | --- | --- | --- |
| id | string | yes | Policy ID |
| amount | float >= 0 | yes | The monthly coverage |
| excess_duration | integer >= 0 | yes | Number of months after which payments start being received |
| sum_cap | float >= 0 | yes | Maximum possible sum received throughout the whole term |
| months_covered | integer >= 0 | yes | For how many months will payments be received after the excess duration is over |
| insured | list[object] | yes | List of adults insured (see Insured schema above) |

###### ESP

| Name | Type | Required | Description |
| --- | --- | --- | --- |
| id | string | yes | Policy ID |
| period_1_months | integer >= 0 | yes | number of months for which period 1 of the policy lasts |
| period_1_proportion | float between 0 and 100 | yes | proportion of the insured person's income they get as monthly coverage during period 1 of the policy |
| period_2_months | integer >= 0 | yes | same as period_1_duration but for period 2 |
| period_2_proportion | float between 0 and 100 | yes | same as period_1_proportion but for period 2 |
| insured | list[object] | yes | List of adults insured (see Insured schema above) |

###### CI

| Name | Type | Required | Description |
| --- | --- | --- | --- |
| id | string | yes | Policy ID |
| amount | float >= 0 | yes | The coverage for the policy |
| excess | float >= 0 | no | Excess. |
| insured | list[object] | yes | List of adults insured (see Insured schema above) |

###### HBP

| Name | Type | Required | Description |
| --- | --- | --- | --- |
| id | string | yes | Policy ID |
| amount | float >= 0 | yes | The coverage for the policy |
| excess | float >= 0 | no | Excess. |
| insured | list[object] | yes | List of residences insured (see Insured schema above) |

###### HCP

| Name | Type | Required | Description |
| --- | --- | --- | --- |
| id | string | yes | Policy ID |
| amount | float >= 0 | yes | The coverage for the policy |
| excess | float >= 0 | no | Excess. |
| specialised_cover | float >= 0 | yes | Specialised item cover. |
| specialised_excess | float >= 0 | no | Excess for specialised items. |
| insured | list[object] | yes | List of residences insured (see Insured schema above) |

###### TRP

| Name | Type | Required | Description |
| --- | --- | --- | --- |
| id | string | yes | Policy ID |
| medical_emergency_and_repatriation_cap | float >= 0 | yes | Cap for medical emergencies and repatriation |
| repatriation_of_remains_cap | float >= 0 | yes | Cap for repatriation of remains. |
| regions | list[int] (at least one region) | yes | List of worldwide region ids for which the travel policy is valid. |
| insured | list[object] | yes | List of adults insured (see Insured schema above) |

###### PSVP

| Name | Type | Required | Description |
| --- | --- | --- | --- |
| id | string | yes | Policy ID |
| amount | float >= 0 | yes | The coverage for the policy |
| excess | float >= 0 | no | Excess. |
| insured | list[object] | yes | List of residences insured (see Insured schema above) |

###### ISVP

| Name | Type | Required | Description |
| --- | --- | --- | --- |
| id | string | yes | Policy ID |
| amount | float >= 0 | yes | The coverage for the policy |
| excess | float >= 0 | no | Excess. |
| insured | list[object] | yes | List of residences insured (see Insured schema above) |

###### CAR

| Name | Type | Required | Description |
| --- | --- | --- | --- |
| id | string | yes | Policy ID |
| excess | float >= 0 | no | Excess. |
| coverage_level | string | yes | Level of coverage: one of "third_party", "fire_theft", "comprehensive". |
| insured | list[object] | yes | List of adults insured (see Insured schema above) |

###### PET

| Name | Type | Required | Description |
| --- | --- | --- | --- |
| id | string | yes | Policy ID |
| amount | float >= 0 | yes | The coverage cap for the policy |
| excess | float >= 0 | no | Excess. |
| insured | list[object] | yes | List of pets insured (see Insured schema above) |