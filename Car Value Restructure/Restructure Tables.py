
# coding: utf-8

# In[1]:

import numpy as np
import pandas as pd


# In[2]:

input_table=pd.read_csv("inputs/CarValueForRestructure.csv")
input_table.columns=["Model","StartDate","EndDate","LowValue","HighValue"]


# In[3]:

one_year_models=input_table[input_table.loc[:,"HighValue"].isnull()].index
input_table.loc[one_year_models,"HighValue"]=input_table.loc[one_year_models,"LowValue"]
input_table.loc[one_year_models,"LowValue"]=input_table.loc[one_year_models,"EndDate"]
input_table.loc[one_year_models,"EndDate"]=input_table.loc[one_year_models,"StartDate"]


# In[4]:

# Page Numbers and Make rows have columns full o NaNs
#Selecting numeric will give us the page numbers
nan_filter=input_table.loc[:,["StartDate","EndDate","LowValue","HighValue"]].isnull().all(axis=1)

page_number_pos=input_table.loc[nan_filter,"Model"].str.isnumeric()
page_number_filters=nan_filter&page_number_pos


# In[5]:

##Car Makes are 1 row below page numbers so creating a filter for them
car_make_filter=page_number_filters.copy()
car_make_filter.index=car_make_filter.index.values+1
car_make_filter[0]=False
car_make_filter.drop(2505,inplace=True)
car_make_filter[0]=False
car_make_filter.sort_index(inplace=True)
car_make_positions=input_table.loc[car_make_filter,"Model"].index


# In[6]:

Make_Series=input_table.loc[car_make_positions,"Model"]


# In[7]:

Make_Series.loc[Make_Series=="APRILIA"]


# In[8]:

output_table=input_table.copy()
output_table["Vehicle_Type"]=0
output_table.loc[:1959,"Vehicle_Type"]="Car"
output_table.loc[1960:,"Vehicle_Type"]="Motorbike"
output_table["Make"]=0
number_of_makes=len(input_table.loc[car_make_filter,"Model"].index)
for i in np.arange(number_of_makes):
  if i==number_of_makes-1:
    output_table.loc[car_make_positions[i]:,"Make"]=Make_Series.loc[car_make_positions[i]]
    print(input_table.loc[car_make_positions[i],"Model"])
  else:
    output_table.loc[car_make_positions[i]:car_make_positions[i+1]-1,"Make"]=Make_Series.loc[car_make_positions[i]]
    print(input_table.loc[car_make_positions[i],"Model"])


# In[9]:

output_table=output_table[['Vehicle_Type','Make','Model', 'StartDate', 'EndDate', 'LowValue', 'HighValue']]
output_table.drop(nan_filter.loc[nan_filter].index.values,inplace=True)


# In[10]:

output_table.reset_index(drop=True,inplace=True)
output_table.to_csv("Outputs/CleanedCarPriceTable.csv",encoding="utf-8")


# In[ ]:




# In[11]:

output_table


# In[ ]:



