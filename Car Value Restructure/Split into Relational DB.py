
# coding: utf-8

# In[40]:

import numpy as np
import pandas as pd


# In[41]:

Full_table=pd.read_csv("Outputs/CleanedCarPriceTable.csv",index_col=0)


# In[42]:

Full_table

Type_Make=Full_table.loc[:,["Vehicle_Type","Make"]]
Make_Model=Full_table.loc[:,["Make","Model"]]
Model_Values=Full_table.loc[:,["Model","StartDate","EndDate","LowValue","HighValue"]]


# In[43]:

Type_Make.drop_duplicates(inplace=True)
Type_Make.reset_index(inplace=True,drop=True)
Type_Make.to_csv("Outputs/TypeMakeLookup.csv")


# In[44]:

Make_Model.drop_duplicates(inplace=True)
Make_Model.reset_index(inplace=True,drop=True)
Make_Model.to_csv("Outputs/MakeModelLookup.csv")


# In[ ]:



